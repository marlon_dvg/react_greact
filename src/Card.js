import React from "react"
import PropTypes from "prop-types"

class Card extends React.Component {

    render() {
        const styles = {
            backgroundColor: this.props.cardColor,
            height: this.props.cardHeight,
            width: this.props.cardWidth
        }

        return (
            <div style={styles}></div>
        )
    }
}

Card.propTypes = {
    cardColor: PropTypes.oneOf(["red", "blue", "yellow", "green"]).isRequired,
    cardHeight: PropTypes.number.isRequired,
    cardWidth: PropTypes.number.isRequired,
}

Card.defaultProps = {
    cardColor: "yellow",
    cardHeight: 100,
    cardWidth: 100
}

export default Card