import React, { Component } from "react"

class Counter extends Component {
    state = {
        counter: 0,
        greeting: "Hello",
        age: 42
    }

    increment = () => {
        this.setState(prevState => ({ counter: prevState.counter + 1 }))
    }

    decrement = () => {
        this.setState(prevState => {
            return {
                counter: prevState.counter - 1
            }
        })
    }

    render() {
        const { counter, greeting, age } = this.state

        return (
            <div>
                <h1>{counter}</h1>
                <button onClick={this.increment}>+</button>
                <button onClick={this.decrement}>-</button>

                <h2>{greeting}</h2>
                <h2>Age: {age}</h2>
            </div>
        )
    }

}

export default Counter